<?php
/**
 * Template Name: Data Archivio
 */
?>

<?php get_header(); ?>
<?php
global $wp_query;
$id = $wp_query->get_queried_object_id();

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

$sidebar = $qode_options_proya['category_blog_sidebar'];

$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments']))
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];

if(isset($qode_options_proya['blog_page_range']) && $qode_options_proya['blog_page_range'] != ""){
	$blog_page_range = $qode_options_proya['blog_page_range'];
} else{
	$blog_page_range = $wp_query->max_num_pages;
}

?>

	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
		var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>

		<?php get_template_part( 'title' ); ?>
		<div class="container">
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                <div class="overlapping_content"><div class="overlapping_content_inner">
            <?php } ?>
			<div class="container_inner default_template_holder clearfix" id="film">
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>


				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
				    <?php if(function_exists('bcn_display')){
				        bcn_display();
				    }?>
				</div>
				
				<!-- Virgilio -->
				<?php 
				$dataConNome = "d/m";
				$giorno = $_GET['d']; 
				?>

				<div id="info-cinema" class="grigio virgilio">
					<h2 id="virgilio" class="rosso">FILM IN SALA DAL <?php echo date_i18n($dataConNome, strtotime($giorno)); ?></h2>
				</div>

				<!-- Tabella Virgilio -->
				<table>
					<thead>
						<tr>
							<th>Film</th>
							<th>Al Cinema</th>
							<th>Orari spettacoli</th>
						</tr>
					</thead>
					<tbody>
						<!-- Film -->
						<?php
				        	
							$args =  array( 'post_type' => array( 'virgilio', 'quantestorie') );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); 
							$field_name = "in_sala_dal";
							$field = get_field_object($field_name);

								
						?>
						<?php if ($field['value'] == $giorno ): ?>
						<tr>
							<td class="film grigio" data-label="Film">
								<div class="locandina">
									<a href="<?php the_permalink() ?>" class="pull-left">
										<?php the_post_thumbnail('thumbnail'); ?>
									</a>
									<div class="locandina-testo">
										<a href="<?php the_permalink() ?>"><h3> <?php echo preg_replace('~((\w+\s){3})~', '$1' . "<br />", get_the_title());?></h3></a>
										<p>
											<?php if( get_field('genere') ): ?>
												
												<?php the_field('genere'); ?>
												
												<?php endif; ?>
												
												<?php if( get_field('durata') ): ?>
												,<?php the_field('durata'); ?>
											<?php endif; ?>
										</p>

										<p class="optional-group show-lg">
											<?php if( get_field('opzione_1') ): ?>
										
											<span class="optional"><?php the_field('opzione_1'); ?></span>
										
											<?php endif; ?>
										</p>

									</div>
								</div>
							</td>
							<td class="al-cinema">
								<span class="rosso"><?php echo get_post_type(); ?></span>
							</td>
							<td class="orari-spettacoli">
								<span class="show-md">Spettacoli: </span>
								<?php if( get_field('orario_1') ): ?>
							
									<span class="orario"><?php the_field('orario_1'); ?></span>
								
								<?php endif; ?>
								<?php if( get_field('orario_2') ): ?>
								
									<span class="orario"><?php the_field('orario_2'); ?></span> 
								
								<?php endif; ?>
								<?php if( get_field('orario_3') ): ?>
								
									<span class="orario"><?php the_field('orario_3'); ?></span>
								
								<?php endif; ?>

								<p class="optional-group show-md">
									<?php if( get_field('opzione_1') ): ?>
								
									<br /><br /><span class="optional"><?php the_field('opzione_1'); ?></span>
								
									<?php endif; ?>
								</p>
							</td>
						</tr>
						<?php endif; ?>

						<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>

						<?php endwhile; ?>
					</tbody>
				</table>
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>
					<div class="<?php if($sidebar == "1"):?>two_columns_66_33<?php elseif($sidebar == "2") : ?>two_columns_75_25<?php endif; ?> background_color_sidebar grid2 clearfix">
						<div class="column1">
							<div class="column_inner">
								<?php 
									get_template_part('templates/blog', 'structure');
								?>
							</div>
						</div>
						<div class="column2">
							<?php get_sidebar(); ?>	
						</div>
					</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
					<div class="<?php if($sidebar == "3"):?>two_columns_33_66<?php elseif($sidebar == "4") : ?>two_columns_25_75<?php endif; ?> background_color_sidebar grid2 clearfix">
						<div class="column1">
						<?php get_sidebar(); ?>	
						</div>
						<div class="column2">
							<div class="column_inner">
								<?php 
									get_template_part('templates/blog', 'structure');
								?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                </div></div>
            <?php } ?>
		</div>
<?php get_footer(); ?>