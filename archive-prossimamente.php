<?php
/**
 * Template Name: Prossimamente Archivio
 */
?>

<?php get_header(); ?>
<?php
global $wp_query;
$id = $wp_query->get_queried_object_id();

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

$sidebar = $qode_options_proya['category_blog_sidebar'];

$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments']))
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];

if(isset($qode_options_proya['blog_page_range']) && $qode_options_proya['blog_page_range'] != ""){
	$blog_page_range = $qode_options_proya['blog_page_range'];
} else{
	$blog_page_range = $wp_query->max_num_pages;
}

?>

	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
		var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>

		<?php get_template_part( 'title' ); ?>
		<div class="container">
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                <div class="overlapping_content"><div class="overlapping_content_inner">
            <?php } ?>
			<div class="container_inner default_template_holder clearfix" id="film">
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>
				
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
				    <?php if(function_exists('bcn_display')){
				        bcn_display();
				    }?>
				</div>

				<!-- Prossimamente -->
				<div id="info-cinema" class="grigio manziana">
					<h2 id="quantestorie" class="rosso">Prossimamente</h2>
				</div>
				<!-- Tabella Prossimamente -->
				<table>
					<thead>
						<tr>
							<th>Film</th>
							<th>In sala</th>
							<th>Al Cinema</th>
						</tr>
					</thead>
					<tbody>
						<!-- Film -->
						<?php
				        	$dataConNome = "d/m";
					    	$dateformatstring = "d F";
							$args = array( 
								'post_type' => 'prossimamente',
							);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); 
						?>
						<tr>
							<td class="film grigio" data-label="Film">
								<div class="locandina">
									<a href="<?php the_permalink() ?>" class="pull-left">
										<?php the_post_thumbnail('thumbnail'); ?>
									</a>
									<div class="locandina-testo">
										<a href="<?php the_permalink() ?>"><h3> <?php echo preg_replace('~((\w+\s){3})~', '$1' . "<br />", get_the_title());?></h3></a>
										<p>
											<?php if( get_field('genere') ): ?>
												
												<?php the_field('genere'); ?>
												
												<?php endif; ?>
												
												<?php if( get_field('durata') ): ?>
												,<?php the_field('durata'); ?>
												<?php the_field('dal_giorno'); ?>
											<?php endif; ?>
										</p>
									</div>
								</div>
							</td>
							<td class="in-sala">
								<span class="show-md">In sala dal</span>
								<span class="show-lg">Dal</span>
								<?php if( get_field('dal_giorno') ): ?>
									<span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('dal_giorno'))); ?></span> 
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('dal_giorno'))); ?></span><br class="show-lg" />
							
								<?php endif; ?>
							</td>
							<td class="orari-spettacoli">
								<span class="show-md">Spettacoli: </span>
								<?php if( get_field('al_cinema') ): ?>
							
									<span class="orario"><?php the_field('al_cinema'); ?></span>
								
								<?php endif; ?>
								
							</td>
						</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			    
				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>
					<div class="<?php if($sidebar == "1"):?>two_columns_66_33<?php elseif($sidebar == "2") : ?>two_columns_75_25<?php endif; ?> background_color_sidebar grid2 clearfix">
						<div class="column1">
							<div class="column_inner">
								<?php 
									get_template_part('templates/blog', 'structure');
								?>
							</div>
						</div>
						<div class="column2">
							<?php get_sidebar(); ?>	
						</div>
					</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
					<div class="<?php if($sidebar == "3"):?>two_columns_33_66<?php elseif($sidebar == "4") : ?>two_columns_25_75<?php endif; ?> background_color_sidebar grid2 clearfix">
						<div class="column1">
						<?php get_sidebar(); ?>	
						</div>
						<div class="column2">
							<div class="column_inner">
								<?php 
									get_template_part('templates/blog', 'structure');
								?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                </div></div>
            <?php } ?>
		</div>
<?php get_footer(); ?>