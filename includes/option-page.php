<?php
require_once('php/rational-option-pages.php');


$pages = array(
    'info-cinema'   => array(
        'page_title'    => __( 'Info Cinema', 'sample-domain' ),
        'position'=> 30,
        'icon_url'=> 'dashicons-info',
        // via the subpages key
        'subpages'      => array(
            'sub-page-one'  => array(
                'page_title'    => __( 'Cinema Quantestorie', 'sample-domain' ),
                'sections'      => array(
		            'section-one'   => array(
		                'title'         => __( 'Manziana', 'sample-domain' ),
		                'fields'        => array(
		                    'default1'       => array(
		                        'title'         => __( 'Indirizzo', 'sample-domain' ),
		                        'text'          => __( 'Inserisci l\'indirzzo' ),

		                    ),
		                    'default2'       => array(
		                        'title'         => __( 'Offerte / Promozioni', 'sample-domain' ),
		                        'text'          => __( 'Inserisci il testo delle promozioni' ),

		                    ),
                        'default3'       => array(
		                        'title'         => __( 'Messaggi Opzionali', 'sample-domain' ),
		                        'text'          => __( 'Inserisci il testo del messaggio' ),

		                    ),
		                    'number'        => array(
		                        'title'         => __( 'Prezzo Intero', 'sample-domain' ),
		                        'type'          => 'text',
		                        'value'         => 20,
		                    ),
		                    'number2'        => array(
		                        'title'         => __( 'Prezzo Ridotto', 'sample-domain' ),
		                        'type'          => 'text',
		                        'value'         => 10,
		                    ),
		                    'tel'           => array(
		                        'title'         => __( 'Segreteria', 'sample-domain' ),
		                        'type'          => 'tel',
		                        'placeholder'   => '06-2121212',
		                    ),
		                ),
		            ),
		        ),
            ),
            'sub-page-two'  => array(
                'page_title'    => __( 'Multisala Virgilio', 'sample-domain' ),
                'sections'      => array(
		            'section-one'   => array(
		                'title'         => __( 'Bracciano', 'sample-domain' ),
		                'fields'        => array(
		                    'default1'       => array(
		                        'title'         => __( 'Indirizzo', 'sample-domain' ),
		                        'text'          => __( 'Inserisci l\'indirzzo' ),

		                    ),
		                    'default2'       => array(
		                        'title'         => __( 'Offerte / Promozioni', 'sample-domain' ),
		                        'text'          => __( 'Inserisci il testo delle promozioni' ),

		                    ),
                        'default3'       => array(
		                        'title'         => __( 'Messaggi Opzionali', 'sample-domain' ),
		                        'text'          => __( 'Inserisci il testo del messaggio' ),

		                    ),
		                    'number'        => array(
		                        'title'         => __( 'Prezzo Intero', 'sample-domain' ),
		                        'type'          => 'text',
		                        'value'         => 20,
		                    ),
		                    'number2'        => array(
		                        'title'         => __( 'Prezzo Ridotto', 'sample-domain' ),
		                        'type'          => 'text',
		                        'value'         => 10,
		                    ),
		                    'tel'           => array(
		                        'title'         => __( 'Segreteria', 'sample-domain' ),
		                        'type'          => 'tel',
		                        'placeholder'   => '06-2121212',
		                    ),
		                ),
		            ),
		        ),
            ),
        ),
    ),


);
$option_page = new RationalOptionPages( $pages );
