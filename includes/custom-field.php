<?php 

// Creo i custom field per i post del cinema
// Li richiamo con the_field('nome_campo')

if(function_exists("register_field_group")){
	// Trama + trailer
	register_field_group(array (
		'id' => 'acf_1-trama-trailer',
		'title' => '1) Trama / Trailer',
		'fields' => array (
			array (
				'key' => 'field_57cea88d26ed6',
				'label' => 'Trama',
				'name' => 'trama',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_57cea89f26ed7',
				'label' => 'Trailer',
				'name' => 'trailer',
				'type' => 'text',
				'instructions' => 'Inserisci il link YouTube',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'quantestorie',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'virgilio',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'prossimamente',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 2,
	));
	
	// scheda film
	register_field_group(array (
		'id' => 'acf_2-scheda-film',
		'title' => '2) Scheda Film',
		'fields' => array (
			array (
				'key' => 'field_57ded4d7301b3',
				'label' => 'Data di uscita',
				'name' => 'data_di_uscita',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57ded4ea301b4',
				'label' => 'Genere',
				'name' => 'genere',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57ded4fd301b5',
				'label' => 'Regia',
				'name' => 'regia',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57ded509301b6',
				'label' => 'Attori',
				'name' => 'attori',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			// array (
			// 	'key' => 'field_57ded520301b7',
			// 	'label' => 'Produzione',
			// 	'name' => 'produzione',
			// 	'type' => 'text',
			// 	'default_value' => '',
			// 	'placeholder' => '',
			// 	'prepend' => '',
			// 	'append' => '',
			// 	'formatting' => 'html',
			// 	'maxlength' => '',
			// ),
			// array (
			// 	'key' => 'field_57ded527301b8',
			// 	'label' => 'Distribuzione',
			// 	'name' => 'distribuzione',
			// 	'type' => 'text',
			// 	'default_value' => '',
			// 	'placeholder' => '',
			// 	'prepend' => '',
			// 	'append' => '',
			// 	'formatting' => 'html',
			// 	'maxlength' => '',
			// ),
			array (
				'key' => 'field_57ded5497a47b',
				'label' => 'Paese',
				'name' => 'paese',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57ded54e7a47c',
				'label' => 'Durata',
				'name' => 'durata',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'quantestorie',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'virgilio',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'prossimamente',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	// Orario+programmazione
	register_field_group(array (
		'id' => 'acf_3-orario-sala-giorni-di-programmazione',
		'title' => '3) Orario / Sala / Giorni di programmazione',
		'fields' => array (
			array (
				'key' => 'field_57cea7901b697',
				'label' => 'Orario 1',
				'name' => 'orario_1',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57d12a993d037',
				'label' => 'Orario 2',
				'name' => 'orario_2',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57d12aab3d038',
				'label' => 'Orario 3',
				'name' => 'orario_3',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_opzione_1',
				'label' => 'Opzione Sala',
				'name' => 'opzione_1',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_57d1299602d8d',
				'label' => 'Sala',
				'name' => 'sala',
				'type' => 'select',
				'choices' => array (
					'Sala 1' => 'Sala 1',
					'Sala 2' => 'Sala 2',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 1,
			),
			array (
				'key' => 'field_57cea7d21b698',
				'label' => 'In sala dal',
				'name' => 'in_sala_dal',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'dd/mm/yy',
				'first_day' => 1,
			),
			array (
				'key' => 'field_57d12c62bf693',
				'label' => 'Al',
				'name' => 'fino_al',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'dd/mm/yy',
				'first_day' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'quantestorie',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'virgilio',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'prossimamente',
					'order_no' => 0,
					'group_no' => 2,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'quantestorie',
					'order_no' => 0,
					'group_no' => 3,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));

	// prossimamente al cinema
	register_field_group(array (
		'id' => 'acf_prossimamente',
		'title' => 'Prossimamente',
		'fields' => array (
			array (
				'key' => 'field_57d1cb05ca0f8',
				'label' => 'Dal giorno',
				'name' => 'dal_giorno',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'dd/mm/yy',
				'first_day' => 1,
			),
			array (
				'key' => 'field_57d1cb1aca0f9',
				'label' => 'Al cinema',
				'name' => 'al_cinema',
				'type' => 'select',
				'choices' => array (
					'virgilio' => 'Virgilio',
					'quantestorie' => 'Quantestorie',
				),
				'default_value' => '',
				'allow_null' => 0,
				'multiple' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'prossimamente',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	
}