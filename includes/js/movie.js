jQuery(document).ready(function($) {

  // POST TYPE SWITCHER
  $('#pts_post_type option:nth-child(1)').attr('value', 'post').hide();
  $('#pts_post_type option:nth-child(2)').attr('value', 'post').hide();
  $('#pts_post_type option:nth-child(3)').attr('value', 'post').hide();

  if($('.wrap h1').append('<br /><button id="magic" type="button" class="magic-button red">Clicca qui per inserire un nuovo film!</button>')){
    $('#magic').on('click', myFunction);
  }else{
    alert('la compilazione automatica potrebbe non funzionare, contattare lo sviluppatore');
  }

  function get_hostname(url) {
    var m = url.match(/^http:\/\/[^/]+/);
    return m ? m[0] : null;
  }
  
  


  function myFunction() {

        var domain_url = get_hostname(window.location.href);
        
        var film = swal({
          title: 'Inserisci link scheda film',
          text: 'esempio: http://filmup.leonardo.it/sc_bridgetjonessbaby.htm',
          input: 'text',
          confirmButtonColor:'#da0707',
          showCancelButton: true,
          inputValidator: function(value) {
            return new Promise(function(resolve, reject) {
              if (value.indexOf("http://filmup.leonardo.it") == -1) {
                reject('Copia la scheda film da http://filmup.leonardo.it/ !');
              } else {
                resolve();
                
              }
            });
          }
        }).then(function(result) {
          $.ajax({
              url: domain_url+'/wp-content/themes/bridge-2_child/includes/proxy.php',
              // url: 'http://frontera.dev/wp-content/themes/bridge-2_child/includes/proxy.php',
              type: 'POST',
              data: {
                  address: result
              },
              success: function(response) {

                  swal({
                    title:"", 
                    text:"Le info sono state recuperate con successo!", 
                    type:"success",
                    confirmButtonColor:'#da0707'
                  });
                  // PRELEVO POSTER
                  var locandina = 'http://filmup.leonardo.it/posters/loc/500/' + $(response).find('table > tbody > tr > td:nth-child(1) > div:nth-child(2) > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > img:nth-child(7)').attr('src').replace('locand/','');
                  
                  
                  // PRELEVO TITOLO + TRAMA
                  var titolo =  $(response).find('table > tbody > tr > td:nth-child(1) > div:nth-child(2) > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > font:nth-child(1)');
                  var trama =   $(response).find('table > tbody > tr > td:nth-child(1) > div:nth-child(2) > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > font:nth-child(6)');
                  var trama = trama.get(0).innerText;
                  var trama = $.trim(trama.replace('Trama:',''));
                  // PRELEVO SCHEDA FILM 
                  
                  var data_di_uscita = $(response).find("font:contains('Data di uscita:')").parent().next().text();
                  var genere = $(response).find("font:contains('Genere:')").parent().next().text();
                  var regista = $(response).find("font:contains('Regia:')").parent().next().text();
                  var cast = $(response).find("font:contains('Cast:')").parent().next().text();
                  // var produzione = $(response).find("font:contains('Produzione:')").parent().next().text();
                  // var distribuzione = $(response).find("font:contains('Distribuzione:')").parent().next().text();
                  var nazione = $(response).find("font:contains('Nazione:')").parent().next().text();
                  var durata = $(response).find("font:contains('Durata:')").parent().next().text();

                  // INSERISCO POSTER
                  $('.poster_input_box').attr('value',locandina)
                  
                  // INSERISCO TITOLO + TRAMA
                  $('#title').focus().attr('value',titolo.get(0).innerText);
                  $('#acf-field-trama').val(trama);

                  // INSERISCO SCHEDA FILM
                  $('#acf-field-data_di_uscita').attr('value',data_di_uscita);
                  $('#acf-field-genere').attr('value', genere);
                  $('#acf-field-regia').attr('value', regista);
                  $('#acf-field-attori').attr('value', cast);
                  // $('#acf-field-produzione').attr('value', produzione);
                  // $('#acf-field-distribuzione').attr('value', distribuzione);
                  $('#acf-field-paese').attr('value', nazione);
                  $('#acf-field-durata').attr('value', durata);
              }
          }); // fine ajax
        }); // fine then
  } // fine funzione

}); //fine document ready
