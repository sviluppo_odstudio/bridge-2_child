<?php

// populate the price column
// $books->populate_column('price', function($column, $post) {
//     echo "£" . get_field('price'); // ACF get_field() function
// }); 
// populate the ratings column
// $books->populate_column('rating', function($column, $post) {
//     echo get_field('rating') . '/5'; // ACF get_field() function
// });
// make rating and price columns sortable
// $books->sortable(array(
//     'price' => array('price', true),
//     'rating' => array('rating', true)
// ));
// use "pages" icon for post type
// $books->menu_icon("dashicons-book-alt");


// Custom post cinema quantestorie
$quantestorie = new CPT(array(
	    'post_type_name' => 'quantestorie',
	    'singular' => 'Quantestorie',
	    'plural' => 'Quantestorie',
	    'slug' => 'quantestorie'
		), array(
		'labels' => array(
			'menu_name' => 'Quantestorie',
			'all_items' => 'Tutti i film',
			'add_new' => 'Aggiungi nuovo',
			'add_new_item' => 'Aggiungi nuovo film',
			'edit_item' => 'Modifica film',
			'new_item' => 'Nuovo film',
			'view_item' => 'Vedi film',
			'search_items' => 'Cerca film',
			'not_found' => 'Non trovato',
			'not_found_in_trash' => 'Non trovato nel cestino'
			),
	    'supports' => array('title', 'thumbnail'),
	    'menu_icon' => get_stylesheet_directory_uri()."/images/custom-post.png"

));



// Gestisco Colonne
$quantestorie->columns(array(
    'cb' => '<input type="checkbox" />',
    'title' => __('Titolo'),
    'featured_image' => __('Poster'),
    'orario_1' => __('Orario 1'),
    'orario_2' => __('Orario 2'),
    'orario_3' => __('Orario 3'),
    'sala'=>__('Sala'),
    'in_sala_dal' => __('In sala dal'),
    'fino_al'=>__('Al')
));

$quantestorie->sortable(array(
	'sala' => array('sala', true),
    'in_sala_dal' => array('in_sala_dal', true),
));

// $quantestorie->menu_icon('dashicons-video-alt3');
// $quantestorie->menu_icon('get_template_directory_uri() . "/images/Logo-stiky.png"');
// Popolo le colonne
$quantestorie->populate_column('orario_1', function($column, $post) {
   echo get_field('orario_1'); // ACF get_field() function
}); 
$quantestorie->populate_column('orario_2', function($column, $post) {
   echo get_field('orario_2'); // ACF get_field() function
}); 
$quantestorie->populate_column('orario_3', function($column, $post) {
   echo get_field('orario_3'); // ACF get_field() function
});
$quantestorie->populate_column('sala', function($column, $post) {
   echo get_field('sala')[0]; // ACF get_field() function
   echo ' <br /> ';
   echo get_field('sala')[1];
}); 
$quantestorie->populate_column('in_sala_dal', function($column, $post) {
	$dateformatstring = "d F";
	echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal')));// ACF get_field() function
}); 
$quantestorie->populate_column('fino_al', function($column, $post) {
	$dateformatstring = "d F";
	echo date_i18n($dateformatstring, strtotime(get_field('fino_al')));// ACF get_field() function
}); 



// Custom post cinema virgilio
$virgilio = new CPT(array(
	    'post_type_name' => 'virgilio',
	    'singular' => 'Virgilio',
	    'plural' => 'Virgilio',
	    'slug' => 'virgilio'
		), array(
		'labels' => array(
			'menu_name' => 'Virgilio',
			'all_items' => 'Tutti i film',
			'add_new' => 'Aggiungi nuovo',
			'add_new_item' => 'Aggiungi nuovo film',
			'edit_item' => 'Modifica film',
			'new_item' => 'Nuovo film',
			'view_item' => 'Vedi film',
			'search_items' => 'Cerca film',
			'not_found' => 'Non trovato',
			'not_found_in_trash' => 'Non trovato nel cestino'
			),

	    'supports' => array('title', 'thumbnail'),
	    'menu_icon' => get_stylesheet_directory_uri()."/images/custom-post.png"
));

$virgilio->sortable(array(
	'sala' => array('sala', true),
    'in_sala_dal' => array('in_sala_dal', true),
));

// $virgilio->menu_icon('dashicons-video-alt3');
// Gestisco Colonne
$virgilio->columns(array(
    'cb' => '<input type="checkbox" />',
    'title' => __('Titolo'),
    'featured_image' => __('Poster'),
    'orario_1' => __('Orario 1'),
    'orario_2' => __('Orario 2'),
    'orario_3' => __('Orario 3'),
    'sala'=>__('Sala'),
    'in_sala_dal' => __('In sala dal'),
    'fino_al'=>__('Al')
));

$virgilio->populate_column('orario_1', function($column, $post) {
   echo get_field('orario_1'); // ACF get_field() function
}); 
$virgilio->populate_column('orario_2', function($column, $post) {
   echo get_field('orario_2'); // ACF get_field() function
}); 
$virgilio->populate_column('orario_3', function($column, $post) {
   echo get_field('orario_3'); // ACF get_field() function
});
$virgilio->populate_column('sala', function($column, $post) {
   echo get_field('sala')[0]; // ACF get_field() function
   echo ' <br /> ';
   echo get_field('sala')[1];
}); 
$virgilio->populate_column('in_sala_dal', function($column, $post) {
   $dateformatstring = "d F";
	echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal')));// ACF get_field() function
}); 
$virgilio->populate_column('fino_al', function($column, $post) {
    $dateformatstring = "d F";
 	echo date_i18n($dateformatstring, strtotime(get_field('fino_al')));// ACF get_field() function
});

// Popolo le colonne
// $virgilio->populate_column('price', function($column, $post) {
//     echo "£" . get_field('price'); // ACF get_field() function
// }); 

// Custom post Prossimamente
$prossimamente = new CPT(array(
	    'post_type_name' => 'prossimamente',
	    'singular' => 'Prossimamente',
	    'plural' => 'Prossimamente',
	    'slug' => 'prossimamente'
		), array(
		'labels' => array(
			'menu_name' => 'Prossimamente in sala',
			'all_items' => 'Tutti i film',
			'add_new' => 'Aggiungi nuovo',
			'add_new_item' => 'Aggiungi nuovo film',
			'edit_item' => 'Modifica film',
			'new_item' => 'Nuovo film',
			'view_item' => 'Vedi film',
			'search_items' => 'Cerca film',
			'not_found' => 'Non trovato',
			'not_found_in_trash' => 'Non trovato nel cestino'
			),

	    'supports' => array('title', 'thumbnail'),
	    'menu_icon' => get_stylesheet_directory_uri()."/images/custom-post.png"
));

// $prossimamente->register_taxonomy(array(
//     'taxonomy_name' => 'cinema',
//     'singular' => 'Cinema',
//     'plural' => 'Cinema',
//     'slug' => 'cinema'
// ));
// $prossimamente->menu_icon('dashicons-video-alt3');
// Gestisco Colonne
$prossimamente->columns(array(
    'cb' => '<input type="checkbox" />',
    'title' => __('Titolo'),
    'featured_image' => __('Poster'),
    'al_cinema'=>__('Al Cinema'),
    'dal_giorno' => __('In sala dal')
));

$prossimamente->populate_column('al_cinema', function($column, $post) {
   echo get_field('al_cinema')[0]; // ACF get_field() function
   echo '<br />';
   echo get_field('al_cinema')[1];
});
$prossimamente->populate_column('dal_giorno', function($column, $post) {
	$dateformatstring = "d F";
	echo date_i18n($dateformatstring, strtotime(get_field('dal_giorno')));// ACF get_field() function
});
// Popolo le colonne
