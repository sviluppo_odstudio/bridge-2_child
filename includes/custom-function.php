<?php  
/**
 * Plugin Name:       Overload Plugin for cinema
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       Aggiunge funzionalità al sito
 * Version:           1.0.0
 * Author:            Overload Design Studio
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       overload-plugin
 */



/** 
 * Snippet Name: Add admin script on custom post types 
 * Snippet URL: http://www.wpcustoms.net/snippets/add-admin-script-on-custom-post-types/ 
 */  
if (!function_exists('wpc_add_admin_cpt_script')) {
	function wpc_add_admin_cpt_script( $hook ) {  
	  
	    global $post;  
	  	// if ( $hook == 'post-new.php' || $hook == 'post.php' || $hook == 'edit.php') {  
	    if ( $hook == 'post-new.php' || $hook == 'post.php') {  
	        if ( 'quantestorie' === $post->post_type || 'virgilio' === $post->post_type || 'prossimamente' === $post->post_type) {

	        	wp_register_script ('movie-script', get_stylesheet_directory_uri() .'/includes/js/movie.js', array( 'jquery' ),'1',true);
	        	
	        	wp_register_script ('sweetalert-script', 'https://cdn.jsdelivr.net/sweetalert2/5.0.7/sweetalert2.min.js', array( 'jquery' ),'1',true);
	        	
	        	wp_register_style ('sweetalert-style', 'https://cdn.jsdelivr.net/sweetalert2/5.0.7/sweetalert2.min.css', array(),'2','all');

	        	wp_register_style ('admin-style', get_stylesheet_directory_uri().'/includes/css/admin.css', array(),'2','all');
	           	
	           	wp_enqueue_script('movie-script');
	          	wp_enqueue_script('sweetalert-script');
				wp_enqueue_style( 'sweetalert-style'); 
				wp_enqueue_style( 'admin-style'); 
	        }  
	    }
	} 
	add_action( 'admin_enqueue_scripts', 'wpc_add_admin_cpt_script', 10, 1 );  	
}

if (!function_exists('remove_wp_logo')) {
	add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

	function remove_wp_logo( $wp_admin_bar ) {
		$wp_admin_bar->remove_node( 'wp-logo' );
	}	
}
if(!function_exists('my_login_logo')){
	function my_login_logo() { ?>
    <style type="text/css">
    	body{
    		background-color: #f9f9f9!important;
    	}
    	#login {
		
		    padding: 2% 0 0!important;
		
		}
		.login #login_error, .login .message{
			border-left: 4px solid #da0707!important;
		}
		.wp-core-ui .button-primary{
			    background: #da0707!important;
			    border-color: #af1c1c!important;
			    -webkit-box-shadow: none!important;
			    box-shadow: none!important;
			    color: #fff;
			    text-decoration: none;
			    text-shadow: 0 -1px 1px #da0707,1px 0 1px #da0707,0 1px 1px #da0707,-1px 0 1px #da0707!important;
		}
		
		.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover{
			    background: #e73838!important;
			    border-color: #af1c1c!important;
			    
		}
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/LoginWP.png);
            background-size: auto;
            -webkik-bacground-size:auto;
            padding-bottom: 120px;
            width: 100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

}

if (!function_exists('my_loginURL')) {
	function my_loginURL() {
	    return 'http://www.cinemabracciano.com/';
	}
	add_filter('login_headerurl', 'my_loginURL');
	
} 



// Aggiungo stili e script nel footer front-end
if(!function_exists('stili_e_script_front_end')){
	function stili_e_script_front_end() {
		//Load JS and CSS files in here
		wp_register_script ('carousel-script', get_stylesheet_directory_uri() .'/js/owl.carousel.min.js', array( 'jquery' ),'1',true);
		wp_register_script ('equal-height-script', get_stylesheet_directory_uri() .'/js/jquery.matchHeight.min.js', array( 'jquery' ),'1',true);
		wp_register_script ('main-script', get_stylesheet_directory_uri().'/js/main.js', array( 'jquery' ),'1',true);

		// FANCY BOX
		wp_register_script ('fancy-script', get_stylesheet_directory_uri().'/includes/source/jquery.fancybox.js?v=2.1.5', array( 'jquery' ),'1',true);
		wp_register_style ('fancy-style', get_stylesheet_directory_uri().'/includes/source/jquery.fancybox.css?v=2.1.5', array(),'2','all');

		// TABELLA DEL FILM + SCHEDA + PROSSIMAMENTE (Comprimerli a fine lavoro)
		wp_register_style ('tabella-film', get_stylesheet_directory_uri().'/css/tabella-film.css', false,'2','all');
		wp_register_style ('prossimamente', get_stylesheet_directory_uri().'/css/prossimamente.css', array(),'2','all');
		wp_register_style ('scheda-film', get_stylesheet_directory_uri().'/css/scheda.css', array(),'2','all');
		wp_register_style ('newsletter', get_stylesheet_directory_uri().'/css/newsletter.css', array(),'2','all');
		wp_register_style ('carousel-style', get_stylesheet_directory_uri().'/css/owl.carousel.css', array(),'2','all');
		wp_register_style ('carousel-theme-style', get_stylesheet_directory_uri().'/css/owl.theme.css', array(),'2','all');
		
		// Carico gli script e gli stili
		wp_enqueue_script('carousel-script');
		wp_enqueue_script('equal-height-script');
		

		// fancybox
		wp_enqueue_script('fancy-script');
		wp_enqueue_style( 'fancy-style');

		// Main 
		wp_enqueue_script('main-script');

		// tabella+scheda+prossimamente
		wp_enqueue_style( 'tabella-film');
		wp_enqueue_style( 'prossimamente');
		wp_enqueue_style( 'scheda-film');
		wp_enqueue_style( 'newsletter');
		wp_enqueue_style( 'carousel-style');
		wp_enqueue_style( 'carousel-theme-style');
	}
	add_action( 'wp_enqueue_scripts', 'stili_e_script_front_end' );

}

// Aggiungo iubenda nel footer
if (!function_exists('add_iubenda')) {

	function add_iubenda(){
		echo '<a href="//www.iubenda.com/privacy-policy/7952694" class="iubenda-white iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>';
	}
	// add_action( 'wp_footer', 'add_iubenda' );
} 
/** 
 * Snippet Name: Cambia nome metabox 
 */
if (!function_exists('change_meta_box_title')) {
	
	function change_meta_box_title() {
	    // Immagine in evidenza
	    remove_meta_box( 'postimagediv', 'quantestorie', 'side' ); //replace post_type from your post type name
	    add_meta_box('postimagediv', __('Carica poster dal computer'), 'post_thumbnail_meta_box', 'quantestorie', 'side', 'high');

	    
		// Immagine in evidenza
	    remove_meta_box( 'postimagediv', 'virgilio', 'side' ); //replace post_type from your post type name
	    add_meta_box('postimagediv', __('Carica poster dal computer'), 'post_thumbnail_meta_box', 'virgilio', 'side', 'high');
	    
	    // Immagine in evidenza
	    remove_meta_box( 'postimagediv', 'prossimamente', 'side' ); //replace post_type from your post type name
	    add_meta_box('postimagediv', __('Carica poster dal computer'), 'post_thumbnail_meta_box', 'prossimamente', 'side', 'high');
	}

	add_action( 'admin_head', 'change_meta_box_title' );
}

// Aggiungo ai poster l'alt tag automaticamente (titolo film)
if (!function_exists('add_alt_tags')) {
	function add_alt_tags($content){
        global $post;
        preg_match_all('/<img (.*?)\/>/', $content, $images);
        if(!is_null($images)){
            foreach($images[1] as $index => $value){
                if(!preg_match('/alt=/', $value)){
                        $new_img = str_replace('<img', '<img alt="'.$post->post_title.'"', $images[0][$index]);
                        $content = str_replace($images[0][$index], $new_img, $content);
                }
            }
        }
        return $content;
	}
	add_filter('the_content', 'add_alt_tags', 99999);	
}

/** Do all'utente la possibilità di gestire le opzioni **/

// if(!function_exists('give_manage_options')){
// 	function give_manage_options() {
// 	    $user = new WP_User( '2' );
// 	    $user->add_cap( 'manage_options');
// 	}
// 	add_action( 'admin_init', 'give_manage_options');
// }

// Aggiungo l'immagine in evidenza nella colonna 
if (!function_exists('custom_columns_data')) {
	function custom_columns_data( $column, $post_id ) {
	    switch ( $column ) {
	    case 'featured_image':
	        echo the_post_thumbnail( array(100,150) );
	        break;
	    }
	}
	add_action( 'manage_posts_custom_column' , 'custom_columns_data', 10, 2 ); 	
}

// ==================================================================
//
// Rimuovo i commenti
//
// ------------------------------------------------------------------


if (!function_exists('df_disable_comments_post_types_support')) {
	// Disable support for comments and trackbacks in post types
	function df_disable_comments_post_types_support() {
		$post_types = get_post_types();
		foreach ($post_types as $post_type) {
			if(post_type_supports($post_type, 'comments')) {
				remove_post_type_support($post_type, 'comments');
				remove_post_type_support($post_type, 'trackbacks');
			}
		}
	}
	add_action('admin_init', 'df_disable_comments_post_types_support');	
}

if(!function_exists('df_disable_comments_status')){
	// Close comments on the front-end
	function df_disable_comments_status() {
		return false;
	}
	add_filter('comments_open', 'df_disable_comments_status', 20, 2);
	add_filter('pings_open', 'df_disable_comments_status', 20, 2);

}

if(!function_exists('df_disable_comments_hide_existing_comments')){
	// Hide existing comments
	function df_disable_comments_hide_existing_comments($comments) {
		$comments = array();
		return $comments;
	}
	add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

}

if(!function_exists('df_disable_comments_admin_menu')){
	// Remove comments page in menu
	function df_disable_comments_admin_menu() {
		remove_menu_page('edit-comments.php');
	}
	add_action('admin_menu', 'df_disable_comments_admin_menu');

}

if(!function_exists('df_disable_comments_admin_menu_redirect')){
	// Redirect any user trying to access comments page
	function df_disable_comments_admin_menu_redirect() {
		global $pagenow;
		if ($pagenow === 'edit-comments.php') {
			wp_redirect(admin_url()); exit;
		}
	}
	add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

}

if(!function_exists('df_disable_comments_dashboard')){
	// Remove comments metabox from dashboard
	function df_disable_comments_dashboard() {
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	}
	add_action('admin_init', 'df_disable_comments_dashboard');

}

if(!function_exists('df_disable_comments_admin_bar')){
	// Remove comments links from admin bar
	function df_disable_comments_admin_bar() {
		if (is_admin_bar_showing()) {
			remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
		}
	}
	add_action('init', 'df_disable_comments_admin_bar');	
}

// ==================================================================
//
// Fine Rimuovo i commenti
//
// ------------------------------------------------------------------

if(!function_exists('hide_update_msg_non_admins')){
	function hide_update_msg_non_admins(){
	 if (!current_user_can( 'activate_plugins' )) { // non-admin users
	        echo '<style>#setting-error-tgmpa{ display: none; }</style>';
	    }
	}
	add_action( 'admin_head', 'hide_update_msg_non_admins');	
}
// Rimuovo dal menu alcune pagine ai non-amministratori
if (!function_exists('my_remove_menu_pages')) {
	function my_remove_menu_pages() {
	
		if (!current_user_can( 'activate_plugins' )) { // non-admin users
			remove_menu_page( 'edit.php' );                   //Posts
			// remove_menu_page( 'edit.php?post_type=page' );    //Pages
			remove_menu_page( 'edit.php?post_type=portfolio_page' );    //Pages
			remove_menu_page( 'edit.php?post_type=testimonials' );    //Pages
			// remove_menu_page( 'edit.php?post_type=slides' );    //Pages
			remove_menu_page( 'edit.php?post_type=carousels' );    //Pages
			remove_menu_page( 'edit.php?post_type=masonry_gallery' );    //Pages
			remove_menu_page( 'edit-comments.php' );          //Comments
			remove_menu_page( 'users.php' );                  //Users
			remove_menu_page( 'tools.php' );                  //Tools
			remove_menu_page( 'options-general.php' );        //Settings
			remove_menu_page( 'edit.php?post_type=page' );    //Pages
			wp_enqueue_style('admin-style', get_stylesheet_directory_uri().'/includes/css/non-admin.css' );  
		}
	}
	add_action( 'admin_menu', 'my_remove_menu_pages' );
}



// Styling for the custom post type icon


// function salva_cat_quantestorie($post_ID) {
// 	global $wpdb;
// 	if(!has_term('','cinema',$post_ID)){
// 		$cat = array(5);
// 		wp_set_object_terms($post_ID, $cat, 'cinema');
// 	}
// }
// add_action('save_post_quantestorie', 'salva_cat_quantestorie');


// function salva_cat_virgilio($post_ID) {
// 	global $wpdb;
// 	if(!has_term('','cinema',$post_ID)){
// 		$cat = array(6);
// 		wp_set_object_terms($post_ID, $cat, 'cinema');
// 	}
// }
// add_action('save_post_virgilio', 'salva_cat_virgilio');

// function salva_cat_prossimamente($post_ID) {
// 	global $wpdb;
// 	if(!has_term('','cinema',$post_ID)){
// 		$cat = array(6);
// 		wp_set_object_terms($post_ID, $cat, 'cinema');
// 	}
// }
// add_action('save_post_prossimamente', 'salva_cat_prossimamente');
?>
