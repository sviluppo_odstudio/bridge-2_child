<?php

include_once( 'php/image_upload_from_url_function.php' );


// Add Input Box for Image URL under the post details box.
if(!function_exists('url_to_image_custom_field')){
	function url_to_image_custom_field() {
	    add_meta_box('url_to_image_meta_box', 'Upload Post Feature Image From URL', 'url_to_image_meta_box', 'post');

	    add_meta_box('url_to_image_meta_box', __('Carica poster da link esterno'), 'url_to_image_meta_box', 'quantestorie');

	    add_meta_box('url_to_image_meta_box', __('Carica poster da link esterno'), 'url_to_image_meta_box', 'virgilio');
	    
	    add_meta_box('url_to_image_meta_box', __('Carica poster da link esterno'), 'url_to_image_meta_box', 'prossimamente');
	}
	 
	function url_to_image_meta_box () {
	 
		// - security -
		echo '<input type="hidden" name="poster-image-url-nonce" id="poster-image-url-nonce" value="' .
		wp_create_nonce( 'poster-image-url-nonce' ) . '" />';
		 

		 
		?>
		<div class="poster_meta_box">
			<ul>
				<li><input placeholder="http://filmup.leonardo.it/posters/loc/500/velocecomeilvento.jpg" class="poster_input_box" name="image_url" value=""></li>
			</ul>
		</div>
		<?php
	}
	add_action( 'admin_init', 'url_to_image_custom_field' );	
}

if(!function_exists('url_to_image_meta_field')){
	function url_to_image_meta_field(){
		global $post;
		 
		$post_id =  $post->ID;

		// Check the security
		 
		if ( !wp_verify_nonce( $_POST['poster-image-url-nonce'], 'poster-image-url-nonce' )) {
		    return $post->ID;
		}
		 
		if ( !current_user_can( 'edit_post', $post->ID )) {
		    return $post->ID;
		} 


		if(!isset($_POST["image_url"])){
			return $post->ID;
		}

		$image_url 	= 	strip_tags($_POST["image_url"]);
		$upload_img =	poster_attach_external_image( $image_url,$post->ID,get_the_title($post->ID));

	}
	add_action ('post_updated', 'url_to_image_meta_field');
}
// Run Image upload from url function
 

if(!function_exists('url_to_image_style_admin')){
	function url_to_image_style_admin() { ?>
		<style type="text/css">
			.poster_meta_box{}
			.poster_input_box{width: 95%;margin: 20px;border:1px solid #ccc;padding:10px}
		</style>
		<?php }
	add_action( 'admin_head', 'url_to_image_style_admin' );	
}

