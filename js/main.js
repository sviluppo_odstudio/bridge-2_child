(function($) {
    'use strict';
    // scroll
    $(function() {
        $('ul#menu-main a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 125
                    }, 1000);
                    return false;
                }
            }
        });

        //Convert address tags to google map links - Copyright Michael Jasper 2011
        $('.indirizzo').each(function() {
            var link = "<a href='http://maps.google.com/maps?q=" + encodeURIComponent($(this).text()) + "' target='_blank'>" + $(this).text() + "</a>";
            $(this).html(link);
        });
        $('.segreteria').each(function() {
            var link = "<a href='tel:+39" + encodeURIComponent($(this).text().replace('/', '').replace(/\./g, '').replace(/ /g, '')) + "'>" + $(this).text() + "</a>";
            $(this).html(link);
        });
        $('img').attr('draggable', 'false');

        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        
        $('.prossimamente').owlCarousel({
            loop: true,
            autoplay: true,
            nav: true,
            navText: ['<i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>', '<i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }

            }
        });

        if (!isMobile.any()) {
            
            // Not is mobile
            $('.item img').matchHeight();
        }
        
        
        if ($('body').hasClass('single')) {

            $('.menu-item a').each(function() {
                var value = $(this).attr('href');
                var res = value.split("#");
                $(this).attr('href', 'http://www.cinemabracciano.com/#' + res[1]);
            });

            $(".video").on('click', function() {
                $.fancybox({
                    'padding'       : 0,
                    'autoScale'     : false,
                    'transitionIn'  : 'none',
                    'transitionOut' : 'none',
                    'title'         : this.title,
                    'width'         : 640,
                    'height'        : 385,
                    'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                    'type'          : 'swf',
                    'swf'           : {
                    'wmode'             : 'transparent',
                    'allowfullscreen'   : 'true'
                    }
                });

                return false;
            });
        }
        
    });

    


})(jQuery);