<?php
/*
Template Name: HOME PAGE
*/
?>
<?php
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider"><div class="q_slider_inner">
			<?php echo do_shortcode($revslider); ?>
			</div></div>
		<?php
		}
		?>

	<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
	<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>
		<?php if(($sidebar == "default")||($sidebar == "")) : ?>

		<!-- TABELLA PER IL CINEMA -->
		<div class="container">
			<div class="container_inner clearfix tabella-film" id="film">

				<!-- Virgilio -->
				<div id="info-cinema" class="grigio virgilio">
					<h2 id="virgilio" class="rosso">MULTISALA VIRGILIO BRACCIANO</h2>
					<div id="indirizzo">
						<?php
							$options = get_option( 'sub-page-two', array() );
							$indirizzo = $options['indirizzo'];
							$offerte_promozioni = $options['offerte_promozioni'];
							$messaggi_opzionali = $options['messaggi_opzionali'];
							$prezzo_intero = $options['prezzo_intero'];
							$prezzo_ridotto = $options['prezzo_ridotto'];
							$segreteria = $options['segreteria'];
					    ?>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i><span class="indirizzo"> <?= $indirizzo; ?></span></p>
						<p><i class="fa fa-phone" aria-hidden="true"></i>Segreteria:<strong><span class="segreteria"><?= $segreteria; ?></span></strong></p>
					</div>
					<div class="biglietti">
						<p class="margin-bottom">
							BIGLIETTO:
							<span>Intero</span>
							<span class="rosso"><?= $prezzo_intero ?>€</span>
							<span class="margin-left">Ridotto</span>
							<span class="rosso"><?= $prezzo_ridotto ?>€</span>
						</p>
						<p class="asterisco margin-top">*Ridotto per bambini sotto gli 8 anni e adulti sopra i 65</p>
					</div>
					<div id="servizi">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/VIRGILIO.png">
					</div>
					<?php if ($offerte_promozioni !=""): ?>
					  	<p class="banner">
				        	<?= $offerte_promozioni; ?>
				      	</p>
				  	<?php endif ?>

						<?php if ($messaggi_opzionali !=""): ?>
						  	<p class="banner_2">
					        	<?= $messaggi_opzionali; ?>
					      </p>
					  <?php endif ?>
				</div>

				<!-- Tabella Virgilio -->
				<table>
					<thead>
						<tr>
							<th>Film</th>
							<th>In sala</th>
							<th>Orari spettacoli</th>
						</tr>
					</thead>
					<tbody>
						<!-- Film -->
						<?php
				        	$dataConNome = "d/m";
					    	$dateformatstring = "d F";
							$args = array(
								'post_type' => 'virgilio',
							);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
						?>
						<tr>
							<td class="film grigio" data-label="Film">
								<div class="locandina">
									<a href="<?php the_permalink() ?>" class="pull-left">
										<?php the_post_thumbnail('thumbnail'); ?>
									</a>
									<div class="locandina-testo">
										<a href="<?php the_permalink() ?>"><h3> <?php echo preg_replace('~((\w+\s){3})~', '$1' . "<br />", get_the_title());?></h3></a>
										<p>
											<?php if( get_field('genere') ): ?>

												<?php the_field('genere'); ?>

												<?php endif; ?>

												<?php if( get_field('durata') ): ?>
												,<?php the_field('durata'); ?>
											<?php endif; ?>
										</p>

										<p class="optional-group show-lg">
											<?php if( get_field('opzione_1') ): ?>

											<span class="optional"><?php the_field('opzione_1'); ?></span>

											<?php endif; ?>
										</p>

										<p class="optional-group show-lg">
											<a href="<?php the_permalink() ?>"><span class="rosso capitalize">Scheda film</span></a>
										</p>


									</div>
								</div>
							</td>
							<td class="in-sala">
								<span class="show-md">In sala dal</span>
								<span class="show-lg">Dal</span>
								<?php if( get_field('in_sala_dal') ): ?>

									<span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('in_sala_dal'))); ?></span>
									<!-- In caso di archivio -->
									<!-- <span class="rosso show-lg"><a href="/data/?d=<?php //echo the_field('in_sala_dal')?>"><?php //echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal'))); ?></a></span><br class="show-lg" /> -->
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal'))); ?></span><br class="show-lg" />

								<?php endif; ?>

								<?php if( get_field('fino_al') ): ?>

									al <span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('fino_al'))); ?></span>
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('fino_al'))); ?></span>

								<?php endif; ?>
							</td>
							<td class="orari-spettacoli">
								<span class="show-md">Spettacoli: </span>
								<?php if( get_field('orario_1') ): ?>

									<span class="orario"><?php the_field('orario_1'); ?></span>

								<?php endif; ?>
								<?php if( get_field('orario_2') ): ?>

									<span class="orario"><?php the_field('orario_2'); ?></span>

								<?php endif; ?>
								<?php if( get_field('orario_3') ): ?>

									<span class="orario"><?php the_field('orario_3'); ?></span>

								<?php endif; ?>

								<p class="optional-group show-md">
									<?php if( get_field('opzione_1') ): ?>

									<br /><br /><span class="optional"><?php the_field('opzione_1'); ?></span>

									<?php endif; ?>
								</p>


							</td>
						</tr>
						<?php endwhile; ?>
					</tbody>
				</table>





				<!-- Quantestorie manziana -->
				<div id="info-cinema" class="grigio manziana">
					<h2 id="quantestorie" class="rosso">Cineteatro Quantestorie Manziana</h2>
					<div id="indirizzo">
						<?php
							$options = get_option( 'sub-page-one', array() );
							$indirizzo = $options['indirizzo'];
							$offerte_promozioni = $options['offerte_promozioni'];
							$messaggi_opzionali = $options['messaggi_opzionali'];
							$prezzo_intero = $options['prezzo_intero'];
							$prezzo_ridotto = $options['prezzo_ridotto'];
							$segreteria = $options['segreteria'];
					    ?>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i><span class="indirizzo"> <?= $indirizzo; ?></span></p>
						<p><i class="fa fa-phone" aria-hidden="true"></i>Segreteria:<strong><span class="segreteria"><?= $segreteria; ?></span></strong></p>
					</div>
					<div class="biglietti">
						<p class="margin-bottom">
							BIGLIETTO:
							<span>Intero</span>
							<span class="rosso"><?= $prezzo_intero ?>€</span>
							<span class="margin-left">Ridotto</span>
							<span class="rosso"><?= $prezzo_ridotto ?>€</span>
						</p>
						<p class="asterisco margin-top">*Ridotto per bambini sotto gli 8 anni e adulti sopra i 65</p>
					</div>
					<div id="servizi">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/VIRGILIO.png">
					</div>
					<?php if ($offerte_promozioni !=""): ?>
					  	<p class="banner">
				        	<?= $offerte_promozioni; ?>
				      </p>
				  	<?php endif ?>

						<?php if ($messaggi_opzionali !=""): ?>
						  	<p class="banner_2">
					        	<?= $messaggi_opzionali; ?>
					      </p>
					  <?php endif ?>
				</div>


			    <!-- Tabella quantestorie -->
				<table>
					<thead>
						<tr>
							<th>Film</th>
							<th>In sala</th>
							<th>Orari spettacoli</th>
						</tr>
					</thead>
					<tbody>
						<!-- Film -->
						<?php
				        	$dataConNome = "d/m";
					    	$dateformatstring = "d F";
							$args = array(
								'post_type' => 'quantestorie',
							);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
						?>
						<tr>
							<td class="film grigio" data-label="Film">
								<div class="locandina">
									<a href="<?php the_permalink() ?>" class="pull-left">
										<?php the_post_thumbnail('thumbnail'); ?>
									</a>
									<div class="locandina-testo">
										<a href="<?php the_permalink() ?>"><h3> <?php echo preg_replace('~((\w+\s){3})~', '$1' . "<br />", get_the_title());?></h3></a>
										<p>
											<?php if( get_field('genere') ): ?>

												<?php the_field('genere'); ?>

												<?php endif; ?>

												<?php if( get_field('durata') ): ?>
												,<?php the_field('durata'); ?>
											<?php endif; ?>
										</p>

										<p class="optional-group show-lg">
											<?php if( get_field('opzione_1') ): ?>

											<span class="optional"><?php the_field('opzione_1'); ?></span>

											<?php endif; ?>
										</p>

										<p class="optional-group show-lg">
											<a href="<?php the_permalink() ?>"><span class="rosso capitalize">Scheda film</span></a>
										</p>

									</div>
								</div>
							</td>
							<td class="in-sala">
								<span class="show-md">In sala dal</span>
								<span class="show-lg">Dal</span>
								<?php if( get_field('in_sala_dal') ): ?>

									<span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('in_sala_dal'))); ?></span>
									<!-- In caso di archivio -->
									<!-- <span class="rosso show-lg"><a href="/data/?d=<?php //echo the_field('in_sala_dal')?>"><?php //echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal'))); ?></a></span><br class="show-lg" /> -->
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal'))); ?></span><br class="show-lg" />

								<?php endif; ?>

								<?php if( get_field('fino_al') ): ?>

									al <span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('fino_al'))); ?></span>
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('fino_al'))); ?></span>

								<?php endif; ?>
							</td>
							<td class="orari-spettacoli">
								<span class="show-md">Spettacoli: </span>
								<?php if( get_field('orario_1') ): ?>

									<span class="orario"><?php the_field('orario_1'); ?></span>

								<?php endif; ?>
								<?php if( get_field('orario_2') ): ?>

									<span class="orario"><?php the_field('orario_2'); ?></span>

								<?php endif; ?>
								<?php if( get_field('orario_3') ): ?>

									<span class="orario"><?php the_field('orario_3'); ?></span>

								<?php endif; ?>

								<p class="optional-group show-md">
									<?php if( get_field('opzione_1') ): ?>

									<br /><br /><span class="optional"><?php the_field('opzione_1'); ?></span>

									<?php endif; ?>
								</p>
							</td>
						</tr>
						<?php endwhile; ?>
					</tbody>
				</table>


			    <h2 id="prox" class="rosso">PROSSIMAMENTE NELLE NOSTRE SALE</h2>

			    <ul class="prossimamente">
			    	<?php
				    	$dataConNome = "d F";
						$args = array( 'post_type' => 'prossimamente');
						$loop = new WP_Query( $args );



						while ( $loop->have_posts() ) : $loop->the_post();
						$field_name = "al_cinema";
						$field = get_field_object($field_name);
					?>
			      <li class="item">

			      	<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('medium'); ?></a>


			        <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>

			        <?php if (get_field('dal_giorno')): ?>
			        	<p>Dal <?php echo date_i18n($dataConNome, strtotime(get_field('dal_giorno'))); ?></p>
			        <?php endif ?>


			        <!-- <p>al  <a href="<?php echo get_post_type_archive_link( $field['value'][0] ); ?>"><span class="rosso capitalize"><?php the_field('al_cinema'); ?></span></a></p> -->
			        <?php if (get_field('al_cinema')): ?>
			        	<p>al  <span class="rosso capitalize"><?php the_field('al_cinema'); ?></span></p>
			        <?php endif ?>

							<p><a href="<?php the_permalink() ?>"><span class="rosso capitalize">Scheda film</span></a></p>

			      </li>
			      <?php endwhile; ?>
			      <!-- more list items -->
			    </ul>
			</div>

			<div class="newsletter">
				<script type="text/javascript">
//<![CDATA[
if (typeof newsletter_check !== "function") {
window.newsletter_check = function (f) {
    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
    if (!re.test(f.elements["ne"].value)) {
        alert("La mail non è corretta");
        return false;
    }
    for (var i=1; i<20; i++) {
    if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
        alert("");
        return false;
    }
    }
    if (f.elements["ny"] && !f.elements["ny"].checked) {
        alert("You must accept the privacy statement");
        return false;
    }
    return true;
}
}
//]]>
</script>

			<div class="newsletter newsletter-subscription">
				<br><br>
				<form method="post" action="http://www.fronteracinemas.com/?na=s" onsubmit="return newsletter_check(this)">

				<table cellspacing="0" cellpadding="3" border="0" id="table-newsletter">
					<thead>
						<h2>Ricevi le news per email</h2>
					</thead>
				<!-- email -->
				<tr>
					<th>Email</th>
					<td align="left"><input class="newsletter-email" type="email" name="ne" size="30" required></td>
				</tr>

				<tr>
					<td colspan="2" class="newsletter-td-submit">
						<input class="newsletter-submit" type="submit" value="Iscrivimi!"/>
					</td>
				</tr>

				</table>
				</form>
				<br><br>
				</div>
			</div>
		</div>
		<!-- FINE TABELLA -->
		<?php elseif($sidebar == "1" || $sidebar == "2"): ?>

			<?php if($sidebar == "1") : ?>
				<div class="two_columns_66_33 clearfix grid2">
					<div class="column1">
			<?php elseif($sidebar == "2") : ?>
				<div class="two_columns_75_25 clearfix grid2">
					<div class="column1">
			<?php endif; ?>
					<?php if (have_posts()) :
						while (have_posts()) : the_post(); ?>
						<div class="column_inner">

						<?php the_content(); ?>
						<?php
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true);
							?>
								</div>
							</div>
							<?php
							}
							?>
						</div>
				<?php endwhile; ?>
				<?php endif; ?>


					</div>
					<div class="column2"><?php get_sidebar();?></div>
				</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
				<?php if($sidebar == "3") : ?>
					<div class="two_columns_33_66 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php elseif($sidebar == "4") : ?>
					<div class="two_columns_25_75 clearfix grid2">
						<div class="column1"><?php get_sidebar();?></div>
						<div class="column2">
				<?php endif; ?>
						<?php if (have_posts()) :
							while (have_posts()) : the_post(); ?>
							<div class="column_inner">
							<?php the_content(); ?>
							<?php
 $args_pages = array(
  'before'           => '<p class="single_links_pages">',
  'after'            => '</p>',
  'pagelink'         => '<span>%</span>'
 );

 wp_link_pages($args_pages); ?>
							<?php
							if($enable_page_comments){
							?>
							<div class="container">
								<div class="container_inner">
							<?php
								comments_template('', true);
							?>
								</div>
							</div>
							<?php
							}
							?>
							</div>
					<?php endwhile; ?>
					<?php endif; ?>


						</div>

					</div>
			<?php endif; ?>
	</div>
	</div>
	<?php get_footer(); ?>
