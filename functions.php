<?php 

define( 'ACF_LITE', true );
include 'includes/advanced-custom-fields/acf.php';
include 'includes/php/CPT.php';
include 'includes/custom-post-type.php';
include 'includes/custom-field.php';
include 'includes/option-page.php';
include 'includes/image-from-url.php';
include 'includes/php/post-type-switcher.php';
include 'includes/custom-function.php';
