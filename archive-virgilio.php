<?php
/**
 * Template Name: Virgilio Archivio
 */
?>

<?php get_header(); ?>
<?php
global $wp_query;
$id = $wp_query->get_queried_object_id();

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

$sidebar = $qode_options_proya['category_blog_sidebar'];

$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments']))
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];

if(isset($qode_options_proya['blog_page_range']) && $qode_options_proya['blog_page_range'] != ""){
	$blog_page_range = $qode_options_proya['blog_page_range'];
} else{
	$blog_page_range = $wp_query->max_num_pages;
}

?>

	<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
		<script>
		var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
		</script>
	<?php } ?>

		<?php get_template_part( 'title' ); ?>
		<div class="container">
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                <div class="overlapping_content"><div class="overlapping_content_inner">
            <?php } ?>
			<div class="container_inner default_template_holder clearfix" id="film">
				<?php if(($sidebar == "default")||($sidebar == "")) : ?>


				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
				    <?php if(function_exists('bcn_display')){
				        bcn_display();
				    }?>
				</div>
				
				<!-- Virgilio -->
				<div id="info-cinema" class="grigio virgilio">
					<h2 id="virgilio" class="rosso">MULTISALA VIRGILIO BRACCIANO</h2>
					<div id="indirizzo">
						<?php  
							$options = get_option( 'sub-page-two', array() );
							$indirizzo = $options['indirizzo'];
							$offerte_promozioni = $options['offerte_promozioni'];
							$prezzo_intero = $options['prezzo_intero'];
							$prezzo_ridotto = $options['prezzo_ridotto'];
							$segreteria = $options['segreteria'];
					    ?>
						<p><i class="fa fa-map-marker" aria-hidden="true"></i><span class="indirizzo"> <?= $indirizzo; ?></span></p>
						<p><i class="fa fa-phone" aria-hidden="true"></i>Segreteria:<strong><span class="segreteria"><?= $segreteria; ?></span></strong></p>
					</div>
					<div class="biglietti">
						<p class="margin-bottom">
							BIGLIETTO:
							<span>Intero</span>
							<span class="rosso"><?= $prezzo_intero ?>€</span>
							<span class="margin-left">Ridotto</span>
							<span class="rosso"><?= $prezzo_ridotto ?>€</span>
						</p>
						<p class="asterisco margin-top">*Ridotto per bambini sotto gli 8 anni e adulti sopra i 65</p>
					</div>
					<div id="servizi">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/VIRGILIO.png">
					</div>
					<?php if ($offerte_promozioni !=""): ?>
					  	<p class="banner">
				        	<?= $offerte_promozioni; ?>
				      	</p>	
				  	<?php endif ?>
				</div>

				<!-- Tabella Virgilio -->
				<table>
					<thead>
						<tr>
							<th>Film</th>
							<th>In sala</th>
							<th>Orari spettacoli</th>
						</tr>
					</thead>
					<tbody>
						<!-- Film -->
						<?php
				        	$dataConNome = "d/m";
					    	$dateformatstring = "d F";
							$args = array( 
								'post_type' => 'virgilio',
							);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); 
						?>
						<tr>
							<td class="film grigio" data-label="Film">
								<div class="locandina">
									<a href="<?php the_permalink() ?>" class="pull-left">
										<?php the_post_thumbnail('thumbnail'); ?>
									</a>
									<div class="locandina-testo">
										<a href="<?php the_permalink() ?>"><h3> <?php echo preg_replace('~((\w+\s){3})~', '$1' . "<br />", get_the_title());?></h3></a>
										<p>
											<?php if( get_field('genere') ): ?>
												
												<?php the_field('genere'); ?>
												
												<?php endif; ?>
												
												<?php if( get_field('durata') ): ?>
												,<?php the_field('durata'); ?>
											<?php endif; ?>
										</p>

										<p class="optional-group show-lg">
											<?php if( get_field('opzione_1') ): ?>
										
											<span class="optional"><?php the_field('opzione_1'); ?></span>
										
											<?php endif; ?>
										</p>

									</div>
								</div>
							</td>
							<td class="in-sala">
								<span class="show-md">In sala dal</span>
								<span class="show-lg">Dal</span>
								<?php if( get_field('in_sala_dal') ): ?>
								
									<span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('in_sala_dal'))); ?></span> 
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('in_sala_dal'))); ?></span><br class="show-lg" />
							
								<?php endif; ?>
								 
								<?php if( get_field('fino_al') ): ?>
							
									al <span class="rosso show-md"><?php echo date_i18n($dataConNome, strtotime(get_field('fino_al'))); ?></span>
									<span class="rosso show-lg"><?php echo date_i18n($dateformatstring, strtotime(get_field('fino_al'))); ?></span> 
							
								<?php endif; ?>
							</td>
							<td class="orari-spettacoli">
								<span class="show-md">Spettacoli: </span>
								<?php if( get_field('orario_1') ): ?>
							
									<span class="orario"><?php the_field('orario_1'); ?></span>
								
								<?php endif; ?>
								<?php if( get_field('orario_2') ): ?>
								
									<span class="orario"><?php the_field('orario_2'); ?></span> 
								
								<?php endif; ?>
								<?php if( get_field('orario_3') ): ?>
								
									<span class="orario"><?php the_field('orario_3'); ?></span>
								
								<?php endif; ?>

								<p class="optional-group show-md">
									<?php if( get_field('opzione_1') ): ?>
								
									<br /><br /><span class="optional"><?php the_field('opzione_1'); ?></span>
								
									<?php endif; ?>
								</p>
							</td>
						</tr>
						<?php endwhile; ?>
					</tbody>
				</table>

				<?php // WP_Query arguments
					$args = array (
						'post_type' => array( 'prossimamente' ),
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {?>
						<h2 id="prox" class="rosso">PROSSIMAMENTE IN QUESTO CINEMA</h2>
						<ul class="prossimamente">
						<?php
						while ( $query->have_posts() ) {
							$query->the_post();
							// do something
							$field_name = "al_cinema";
							$field = get_field_object($field_name);
							$dataConNome = "d F";

							if ($field['value'][0] == 'Virgilio') {?>
							<li class="item">
								<?php the_post_thumbnail('medium'); ?>
								<h4><?php the_title(); ?></h4>
								<p>Dal <?php echo date_i18n($dataConNome, strtotime(get_field('dal_giorno'))); ?></p>
								<p>al <span class="rosso capitalize"><?php the_field('al_cinema'); ?></span></p>
							</li>
							<?php
							} 
						}
						?>
						</ul>
						<?php } 
						// Restore original Post Data
						wp_reset_postdata();
					?>

				<?php elseif($sidebar == "1" || $sidebar == "2"): ?>
					<div class="<?php if($sidebar == "1"):?>two_columns_66_33<?php elseif($sidebar == "2") : ?>two_columns_75_25<?php endif; ?> background_color_sidebar grid2 clearfix">
						<div class="column1">
							<div class="column_inner">
								<?php 
									get_template_part('templates/blog', 'structure');
								?>
							</div>
						</div>
						<div class="column2">
							<?php get_sidebar(); ?>	
						</div>
					</div>
			<?php elseif($sidebar == "3" || $sidebar == "4"): ?>
					<div class="<?php if($sidebar == "3"):?>two_columns_33_66<?php elseif($sidebar == "4") : ?>two_columns_25_75<?php endif; ?> background_color_sidebar grid2 clearfix">
						<div class="column1">
						<?php get_sidebar(); ?>	
						</div>
						<div class="column2">
							<div class="column_inner">
								<?php 
									get_template_part('templates/blog', 'structure');
								?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
            <?php if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes') {?>
                </div></div>
            <?php } ?>
		</div>
<?php get_footer(); ?>